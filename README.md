# Lab 2 -- Linter and SonarQube as a part of quality gates

![pipeline_status](https://gitlab.com/sqr-iu/s23-lab2-linter-and-sonar-as-a-part-of-qg/badges/master/pipeline.svg)

## Solution

![sonarqube](.attachments/1.png)

![sonarcloud](.attachments/2.png)
